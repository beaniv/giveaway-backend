package com.beaniv.giveaway.authentication.controller;

import com.beaniv.giveaway.authentication.service.AuthenticationService;
import com.beaniv.giveaway.model.dto.user.Credentials;
import com.beaniv.giveaway.model.dto.user.TokenDto;
import com.beaniv.giveaway.model.dto.user.UserRegistrationDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthenticationControllerTest {
    @Autowired
    private AuthenticationController authenticationController;

    @MockBean
    private AuthenticationService authenticationService;

    @Test
    void signIn() {
        final String LOGIN = "example_login@gmail.com";
        final String PASSWORD = "example_password";
        Credentials credentials = new Credentials();
        credentials.setLogin(LOGIN);
        credentials.setPassword(PASSWORD);

        final String TOKEN = "h354l66njk25b7mb5o...54t2sb6k";

        Mockito.when(authenticationService.generateToken(credentials))
                .thenReturn(TOKEN);

        TokenDto tokenDtoReturned = authenticationController.signIn(credentials);

        assertThat(tokenDtoReturned.getToken(), is(equalTo(TOKEN)));

        Mockito.verify(authenticationService, Mockito.times(1))
                .generateToken(credentials);
    }

    @Test
    void signUp() {
        final String EMAIL = "example_login@gmail.com";
        final String PASSWORD = "example_password";
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setEmail(EMAIL);
        userRegistrationDto.setPassword(PASSWORD);

        authenticationController.signUp(userRegistrationDto);

        Mockito.verify(authenticationService, Mockito.times(1))
                .registerUser(userRegistrationDto);
    }
}
