package com.beaniv.giveaway.authentication.security.user.service;

import com.beaniv.giveaway.model.entity.User;
import com.beaniv.giveaway.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DefaultUserServiceTest {
    @Autowired
    DefaultUserService defaultUserService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    void register() {
        final String EMAIL = "example_email@gmail.com";
        final String PASSWORD = "example_password";

        final String ENCODED_PASSWORD = "asdgt34tu48esjrgio358";

        User user = new User();
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);

        Mockito.when(passwordEncoder.encode(user.getPassword()))
                .thenReturn(ENCODED_PASSWORD);
        Mockito.when(userRepository.save(user))
                .thenReturn(user);

        User userReturned = defaultUserService.register(user);

        assertThat(userReturned.getEmail(), is(equalTo(EMAIL)));
        assertThat(userReturned.getPassword(), is(equalTo(ENCODED_PASSWORD)));

        Mockito.verify(passwordEncoder, Mockito.times(1))
                .encode(PASSWORD);
        Mockito.verify(userRepository, Mockito.times(1))
                .save(user);
    }

    @Test
    void getAll() {
        final List<User> userList = new ArrayList<>();

        Mockito.when(userRepository.findAll())
                .thenReturn(userList);

        List<User> userListReturned = defaultUserService.getAll();

        assertThat(userListReturned, is(equalTo(userList)));

        Mockito.verify(userRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void findByEmail() {
        final String EMAIL = "example_email@gmail.com";

        User user = new User();
        user.setEmail(EMAIL);

        Mockito.when(userRepository.findByEmail(EMAIL))
                .thenReturn(user);

        User userReturned = defaultUserService.findByEmail(EMAIL);

        assertThat(userReturned.getEmail(), is(equalTo(EMAIL)));

        Mockito.verify(userRepository, Mockito.times(1))
                .findByEmail(EMAIL);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void findByUserId(boolean isExist) {
        final int ID = 1;

        User user = null;
        if (isExist) {
            user = new User();
            user.setId(ID);
        }

        Mockito.when(userRepository.findById(ID))
                .thenReturn(user);

        User userReturned = defaultUserService.findByUserId(ID);

        if (isExist) {
            assertThat(userReturned.getId(), is(equalTo(ID)));
        } else {
            assertThat(userReturned, nullValue());
        }

        Mockito.verify(userRepository, Mockito.times(1))
                .findById(ID);
    }

    @Test
    void delete() {
        final int ID = 1;

        defaultUserService.delete(ID);

        Mockito.verify(userRepository, Mockito.times(1))
                .deleteById(ID);
    }
}
