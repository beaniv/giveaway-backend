package com.beaniv.giveaway.authentication.security;


import com.beaniv.giveaway.authentication.security.user.service.UserService;
import com.beaniv.giveaway.model.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class JwtUserDetailServiceTest {
    @Autowired
    private JwtUserDetailService jwtUserDetailService;

    @MockBean
    private UserService userService;

    @Test
    void loadUserByUsername() {
        final String EMAIL = "example_email@gmail.com";
        final String PASSWORD = "example_password";

        User user = new User();
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);

        Mockito.when(userService.findByEmail(EMAIL))
                .thenReturn(user);

        UserDetails userDetails = jwtUserDetailService.loadUserByUsername(EMAIL);

        assertThat(userDetails.getPassword(), is(equalTo(PASSWORD)));

        Mockito.verify(userService, Mockito.times(1))
                .findByEmail(EMAIL);
    }

    @Test
    void loadUserByNotExistingUsername() {
        final String EMAIL = "example_email@gmail.com";

        Mockito.when(userService.findByEmail(EMAIL))
                .thenReturn(null);

        try {
            jwtUserDetailService.loadUserByUsername(EMAIL);
        } catch (Exception exc) {
            assertThat(exc.getClass(), is(equalTo(UsernameNotFoundException.class)));
        }

        Mockito.verify(userService, Mockito.times(1))
                .findByEmail(EMAIL);
    }
}
