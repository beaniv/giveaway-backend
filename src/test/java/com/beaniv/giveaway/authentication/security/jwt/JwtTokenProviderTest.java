package com.beaniv.giveaway.authentication.security.jwt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class JwtTokenProviderTest {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @MockBean
    private UserDetailsService userDetailsService;

    @Value("${jwt.token.secret}")
    private String secret;

    @Value("${jwt.token.expired}")
    private Long validityInMilliSeconds;

    @Test
    void getUsername() {
        final String LOGIN = "example_login";
        String token = jwtTokenProvider.createToken(LOGIN);

        assertThat(token, is(not(equalTo(LOGIN))));

        String loginFromToken = jwtTokenProvider.getUsername(token);

        assertThat(loginFromToken, is(equalTo(LOGIN)));
    }

    @Test
    void getAuthentication() {
        final String LOGIN = "example_login";
        String token = jwtTokenProvider.createToken(LOGIN);

        final int ID = 1;
        final String PASSWORD = "password_example";
        final String EMAIL = "email_example";
        final Date RESET_DATE = null;
        JwtUser jwtUser = new JwtUser(ID, PASSWORD, EMAIL, RESET_DATE);

        Mockito.when(userDetailsService.loadUserByUsername(LOGIN))
                .thenReturn(jwtUser);

        jwtTokenProvider.getAuthentication(token);

        Mockito.verify(userDetailsService, Mockito.times(1))
                .loadUserByUsername(LOGIN);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void validateToken(boolean isValid) {
        final String LOGIN = "example_login";
        String token = isValid ? jwtTokenProvider.createToken(LOGIN): "aaaaaaaaaaaaaaaa";
        boolean result;

        try {
            result = jwtTokenProvider.validateToken(token);
            assertThat(result, is(true));
        } catch (Exception exc) {
            assertThat(exc.getClass(), is(equalTo(JwtAuthenticationException.class)));
        }
    }

    @Test
    void resolveTokenCorrect() {
        final String token = "asf3tsg...1356dfg";
        final String correctTokenHeader = "Bearer " + token;

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeader("Authorization"))
                .thenReturn(correctTokenHeader);

        String tokenReturned = jwtTokenProvider.resolveToken(request);

        assertThat(tokenReturned, is(equalTo(token)));
    }

    @Test
    void resolveTokenIncorrect() {
        final String token = "asf3tsg...1356dfg";

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getHeader("Authorization"))
                .thenReturn(token);

        String tokenReturned = jwtTokenProvider.resolveToken(request);

        assertThat(tokenReturned, is(nullValue()));
    }

    @Test
    void getSecret() {
        String secretReturned = jwtTokenProvider.getSecret();
        assertThat(secretReturned, is(equalTo(secret)));
    }

    @Test
    void getValidityInMilliSeconds() {
        Long validityInMilliSecondsReturned = jwtTokenProvider.getValidityInMilliSeconds();
        assertThat(validityInMilliSecondsReturned, is(equalTo(validityInMilliSeconds)));
    }
}
