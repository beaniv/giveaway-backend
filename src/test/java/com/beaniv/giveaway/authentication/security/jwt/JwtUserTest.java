package com.beaniv.giveaway.authentication.security.jwt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class JwtUserTest {
    private static final int ID = 1;
    private static final String PASSWORD = "password_example";
    private static final String EMAIL = "email_example";
    private static final Date RESET_DATE = null;

    private JwtUser jwtUser;

    @BeforeEach
    void setUp() {
        jwtUser = new JwtUser(ID, PASSWORD, EMAIL, RESET_DATE);
    }

    @Test
    void getAuthorities() {
        assertThat(jwtUser.getAuthorities(), is(empty()));
    }

    @Test
    void getPassword() {
        assertThat(jwtUser.getPassword(), is(equalTo(PASSWORD)));
    }

    @Test
    void getUsername() {
        assertThat(jwtUser.getUsername(), is(nullValue()));
    }

    @Test
    void isAccountNonExpired() {
        assertThat(jwtUser.isAccountNonExpired(), is(true));
    }

    @Test
    void isAccountNonLocked() {
        assertThat(jwtUser.isAccountNonLocked(), is(true));
    }

    @Test
    void isCredentialsNonExpired() {
        assertThat(jwtUser.isCredentialsNonExpired(), is(true));
    }

    @Test
    void isEnabled() {
        assertThat(jwtUser.isEnabled(), is(true));
    }

    @Test
    void getEmail() {
        assertThat(jwtUser.getEmail(), is(equalTo(EMAIL)));
    }

    @Test
    void getLastPasswordResetData() {
        assertThat(jwtUser.getLastPasswordResetData(), is(equalTo(RESET_DATE)));
    }
}
