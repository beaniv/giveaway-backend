package com.beaniv.giveaway.posts;

import com.beaniv.giveaway.model.dto.post.DetailedPostDto;
import com.beaniv.giveaway.model.dto.post.HomescreenPostDto;
import com.beaniv.giveaway.model.entity.Post;
import com.beaniv.giveaway.model.entity.User;
import com.beaniv.giveaway.repository.PostRepository;
import com.beaniv.giveaway.repository.UserRepository;
import com.beaniv.giveaway.util.dtotransformservice.DtoTransformService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class DefaultPostsServiceTest {

    @Autowired
    private DefaultPostsService defaultPostsService;

    @MockBean
    private DtoTransformService dtoTransformService;

    @MockBean
    private PostRepository postRepository;

    @MockBean
    private UserRepository userRepository;

    @Test
    void getPosts() {
        Set<Post> posts = new HashSet<>();
        Set<HomescreenPostDto> homescreenPostDtoSet = new HashSet<>();

        Mockito.when(postRepository.findAllBy()).thenReturn(posts);
        Mockito.when(dtoTransformService.convertToSetHomescreenPostDto(posts))
                .thenReturn(homescreenPostDtoSet);

        Set<HomescreenPostDto> homescreenPostDtoSetReturned = defaultPostsService.getPosts();

        assertThat(homescreenPostDtoSetReturned, is(equalTo(homescreenPostDtoSet)));

        Mockito.verify(postRepository, Mockito.times(1))
                .findAllBy();
        Mockito.verify(dtoTransformService, Mockito.times(1))
                .convertToSetHomescreenPostDto(posts);
    }

    @Test
    void getUserPosts() {
        User user = new User();
        Set<Post> posts = new HashSet<>();
        user.setPosts(posts);
        Set<HomescreenPostDto> homescreenPostDtoSet = new HashSet<>();

        Mockito.when(userRepository.findById(ArgumentMatchers.anyInt())).thenReturn(user);
        Mockito.when(dtoTransformService.convertToSetHomescreenPostDto(posts)).thenReturn(homescreenPostDtoSet);

        Set<HomescreenPostDto> homescreenPostDtoSetReturned = defaultPostsService.getUserPosts(0);

        assertThat(homescreenPostDtoSetReturned, is(equalTo(homescreenPostDtoSet)));

        Mockito.verify(userRepository, Mockito.times(1))
                .findById(ArgumentMatchers.anyInt());
        Mockito.verify(dtoTransformService, Mockito.times(1))
                .convertToSetHomescreenPostDto(posts);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void getPost(boolean isFinished) {
        Post post = new Post();
        Set<User> users = new HashSet<>();
        post.setUsers(users);
        final long MS_DAY = 24 * 60 * 60 * 1000;
        Timestamp timestamp = isFinished ? new Timestamp(0) : new Timestamp(System.currentTimeMillis() + MS_DAY);
        post.setFinishTime(timestamp);
        DetailedPostDto detailedPostDto = new DetailedPostDto();

        int userId = 0;
        int postId = 0;

        Mockito.when(postRepository.findById(postId)).thenReturn(post);
        Mockito.when(dtoTransformService
                .convertToDetailedPostDto(ArgumentMatchers.any(Post.class), ArgumentMatchers.eq(userId)))
                .thenReturn(detailedPostDto);

        DetailedPostDto detailedPostDtoReturned = defaultPostsService.getPost(postId, userId);

        assertThat(detailedPostDtoReturned, is(equalTo(detailedPostDto)));

        Mockito.verify(postRepository, Mockito.times(1))
                .findById(postId);
        int timesPostSave = isFinished ? 1 : 0;
        Mockito.verify(postRepository, Mockito.times(timesPostSave))
                .save(ArgumentMatchers.any(Post.class));
        Mockito.verify(dtoTransformService, Mockito.times(1))
                .convertToDetailedPostDto(ArgumentMatchers.any(Post.class), ArgumentMatchers.eq(userId));
    }

    @Test
    void getPostWithWinner() {
        Post post = new Post();
        Set<User> users = new HashSet<>();
        User user = new User();
        users.add(user);
        post.setUsers(users);
        post.setFinishTime(new Timestamp(0));
        DetailedPostDto detailedPostDto = new DetailedPostDto();

        int userId = 0;
        int postId = 0;

        Mockito.when(postRepository.findById(postId)).thenReturn(post);
        Mockito.when(dtoTransformService
                .convertToDetailedPostDto(ArgumentMatchers.any(Post.class), ArgumentMatchers.eq(userId)))
                .thenReturn(detailedPostDto);

        DetailedPostDto detailedPostDtoReturned = defaultPostsService.getPost(postId, userId);

        assertThat(detailedPostDtoReturned, is(equalTo(detailedPostDto)));

        Mockito.verify(postRepository, Mockito.times(1))
                .findById(postId);
        Mockito.verify(postRepository, Mockito.times(1))
                .save(ArgumentMatchers.any(Post.class));
        Mockito.verify(dtoTransformService, Mockito.times(1))
                .convertToDetailedPostDto(ArgumentMatchers.any(Post.class), ArgumentMatchers.eq(userId));
    }

    @Test
    void addPost() {
        Post post = new Post();

        defaultPostsService.addPost(post);

        Mockito.verify(postRepository, Mockito.times(1)).save(post);
    }

    @Test
    void addUserToPost() {
        User user = new User();
        Set<Post> posts = new HashSet<>();
        user.setPosts(posts);
        Post post = new Post();

        int userId = 0;
        int postId = 0;

        Mockito.when(userRepository.findById(userId)).thenReturn(user);
        Mockito.when(postRepository.findById(postId)).thenReturn(post);

        defaultPostsService.addUserToPost(userId, postId);

        assertThat(user.getPosts().size(), is(equalTo(1)));
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    @Test
    void removeUserFromPost() {
        User user = new User();
        Set<Post> posts = new HashSet<>();
        Post post = new Post();
        posts.add(post);
        user.setPosts(posts);

        int userId = 0;
        int postId = 0;

        Mockito.when(userRepository.findById(userId)).thenReturn(user);
        Mockito.when(postRepository.findById(postId)).thenReturn(post);

        defaultPostsService.removeUserFromPost(userId, postId);

        assertThat(user.getPosts().size(), is(equalTo(0)));
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(postRepository, Mockito.times(1)).findById(postId);
        Mockito.verify(userRepository, Mockito.times(1)).save(user);

        userRepository.save(user);
    }

    @ParameterizedTest
    @CsvSource({
            "false, true, true, true",
            "true, false, true, true",
            "true, true, false, true",
            "true, true, true, false",
    })
    void rerollWinnerFail(boolean hasWinner, boolean hasUsers, boolean isFinished, boolean isCreator) {
        Post post = new Post();
        final int CREATOR_ID = isCreator ? 0 : 1;
        final String WINNER_EMAIL = hasUsers ? (hasWinner ? "example@gmail.com" : "No winner") : null;
        final long MS_DAY = 24 * 60 * 60 * 1000;
        final Timestamp timestamp = isFinished ? new Timestamp(0) : new Timestamp(System.currentTimeMillis() + MS_DAY);
        post.setWinnerEmail(WINNER_EMAIL);
        post.setFinishTime(timestamp);
        post.setCreatorId(CREATOR_ID);

        int userId = 0;
        int postId = 0;

        Mockito.when(postRepository.findById(postId)).thenReturn(post);

        defaultPostsService.rerollWinner(userId, postId);

        Mockito.verify(userRepository, Mockito.times(0))
                .findByEmail(ArgumentMatchers.anyString());
        Mockito.verify(postRepository, Mockito.times(1))
                .findById(postId);
        Mockito.verify(postRepository, Mockito.times(0))
                .save(post);
    }

    @Test
    void rerollWinner() {
        Post post = new Post();
        Set<User> users = new HashSet<>();
        Set<Post> posts = new HashSet<>();
        final int WINNER_ID = 1;
        final String WINNER_EMAIL = "example@gmail.com";
        posts.add(post);
        User user = new User();
        users.add(user);
        user.setPosts(posts);
        user.setEmail(WINNER_EMAIL);
        user.setId(WINNER_ID);
        final int POST_ID = 0;
        final int CREATOR_ID = 0;
        post.setId(POST_ID);
        post.setWinnerEmail(WINNER_EMAIL);
        post.setFinishTime(new Timestamp(0));
        post.setCreatorId(CREATOR_ID);
        post.setUsers(users);

        Mockito.when(userRepository.findByEmail(WINNER_EMAIL)).thenReturn(user);
        Mockito.when(userRepository.findById(WINNER_ID)).thenReturn(user);
        Mockito.when(postRepository.findById(POST_ID)).thenReturn(post);

        defaultPostsService.rerollWinner(CREATOR_ID, POST_ID);

        Mockito.verify(userRepository, Mockito.times(2))
                .findByEmail(ArgumentMatchers.anyString());
        Mockito.verify(userRepository, Mockito.times(1))
                .findById(ArgumentMatchers.anyInt());
        Mockito.verify(userRepository, Mockito.times(1))
                .save(user);
        Mockito.verify(postRepository, Mockito.times(2))
                .findById(POST_ID);
        Mockito.verify(postRepository, Mockito.times(1))
                .save(post);
    }
}
