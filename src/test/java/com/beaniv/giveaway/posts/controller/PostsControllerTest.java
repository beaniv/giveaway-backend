package com.beaniv.giveaway.posts.controller;

import com.beaniv.giveaway.authentication.security.jwt.JwtUser;
import com.beaniv.giveaway.model.dto.post.DetailedPostDto;
import com.beaniv.giveaway.model.dto.post.HomescreenPostDto;
import com.beaniv.giveaway.model.dto.post.PostIdDto;
import com.beaniv.giveaway.model.dto.post.RegisterPostDto;
import com.beaniv.giveaway.model.entity.Post;
import com.beaniv.giveaway.posts.PostsService;
import com.beaniv.giveaway.util.dtotransformservice.DtoTransformService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PostsControllerTest {

    @Autowired
    private PostsController postsController;

    @MockBean
    private PostsService postsService;

    @MockBean
    private DtoTransformService dtoTransformService;

    @Test
    void getPosts() {
        Set<HomescreenPostDto> homescreenPostDtoSet = new HashSet<>();

        Mockito.when(postsService.getPosts()).thenReturn(homescreenPostDtoSet);

        Set<HomescreenPostDto> homescreenPostDtoSetReturned = postsController.getPosts();

        assertThat(homescreenPostDtoSetReturned, is(equalTo(homescreenPostDtoSet)));

        Mockito.verify(postsService, Mockito.times(1))
                .getPosts();
    }

    @Test
    void getMyPosts() {
        final int ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        JwtUser jwtUser = new JwtUser(ID, PASSWORD, EMAIL, DATE);
        Set<HomescreenPostDto> homescreenPostDtoSet = new HashSet<>();

        Mockito.when(postsService.getUserPosts(ID)).thenReturn(homescreenPostDtoSet);

        Set<HomescreenPostDto> homescreenPostDtoSetReturned = postsController.getMyPosts(jwtUser);

        assertThat(homescreenPostDtoSetReturned, is(equalTo(homescreenPostDtoSet)));

        Mockito.verify(postsService, Mockito.times(1))
                .getUserPosts(ID);
    }

    @Test
    void getPost() {
        final int USER_ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        final String POST_ID = "0";
        JwtUser jwtUser = new JwtUser(USER_ID, PASSWORD, EMAIL, DATE);
        DetailedPostDto detailedPostDto = new DetailedPostDto();

        Mockito.when(postsService.getPost(ArgumentMatchers.anyInt(), ArgumentMatchers.eq(USER_ID)))
                .thenReturn(detailedPostDto);

        DetailedPostDto detailedPostDtoReturned = postsController.getPost(jwtUser, POST_ID);

        assertThat(detailedPostDtoReturned, is(equalTo(detailedPostDto)));

        Mockito.verify(postsService, Mockito.times(1))
                .getPost(ArgumentMatchers.anyInt(), ArgumentMatchers.eq(USER_ID));
    }

    @Test
    void addPost() {
        final int USER_ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        JwtUser jwtUser = new JwtUser(USER_ID, PASSWORD, EMAIL, DATE);
        RegisterPostDto registerPostDto = new RegisterPostDto();
        Post post = new Post();

        Mockito.when(dtoTransformService.convertToPost(registerPostDto))
                .thenReturn(post);

        postsController.addPost(jwtUser, registerPostDto);

        Mockito.verify(dtoTransformService, Mockito.times(1))
                .convertToPost(registerPostDto);
        Mockito.verify(postsService, Mockito.times(1))
                .addPost(post);
    }

    @Test
    void addUserToPost() {
        final int USER_ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        JwtUser jwtUser = new JwtUser(USER_ID, PASSWORD, EMAIL, DATE);
        PostIdDto postIdDto = new PostIdDto();

        postsController.addUserToPost(jwtUser, postIdDto);

        Mockito.verify(postsService, Mockito.times(1))
                .addUserToPost(USER_ID, postIdDto.getId());
    }

    @Test
    void removeUserFromPost() {
        final int USER_ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        JwtUser jwtUser = new JwtUser(USER_ID, PASSWORD, EMAIL, DATE);
        PostIdDto postIdDto = new PostIdDto();

        postsController.removeUserFromPost(jwtUser, postIdDto);

        Mockito.verify(postsService, Mockito.times(1))
                .removeUserFromPost(USER_ID, postIdDto.getId());
    }

    @Test
    void rerollWinner() {
        final int USER_ID = 0;
        final String PASSWORD = "example_password";
        final String EMAIL = "example_email";
        final Date DATE = new Date();
        JwtUser jwtUser = new JwtUser(USER_ID, PASSWORD, EMAIL, DATE);
        PostIdDto postIdDto = new PostIdDto();

        postsController.rerollWinner(jwtUser, postIdDto);

        Mockito.verify(postsService, Mockito.times(1))
                .rerollWinner(USER_ID, postIdDto.getId());
    }
}
